# Parallel Universe

---

## How do I play?

Running the game
- Just clone the repository, and run the `index.html` file in a modern browser.

Play the game (keys, etc)
- Movement:          WASD/Arrows keys.
- Focus:             s, Down Arrow
- Respawn:           s, Down Arrow, r
- Pause:             Escape (esc)

# TODO

Maybe fix bugs

## NOTES

This is an old project, the code is sloppy, but the game is great. Enjoy.
